import 'package:test/test.dart';

import 'package:private_kanban/bloc/application_bloc.dart';
import 'package:private_kanban/state/application_state.dart';

import 'package:private_kanban/db/app_db.dart';

void main() {
  ApplicationBloc applicationBloc;
  final AppDatabase _appDatabase = AppDatabase.get();

  setUp(() {
    applicationBloc = ApplicationBloc(_appDatabase);
  });

  test('initial state is correct', () {
    expect(applicationBloc.initialState, ApplicationState.initializing());
  });

  test('dispose does not emit new states', () {
    expect(applicationBloc.initialState, ApplicationState.initializing());
    applicationBloc.dispose();
  });

  tearDown(() async {
    await _appDatabase.close();
  });
}
