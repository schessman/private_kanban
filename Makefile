DCMD=find lib -type f -print | grep dart$ | sort
DARTLIST:=$(shell $(DCMD) )
DLINT=lint.txt
CTAGS=/usr/local/bin/ctags_for_dart.py

.PHONY: list lint run test
all: list tags lint

run:
	-flutter run |& tee run.out

test:
	-flutter test |& tee test.out

list:
	$(DCMD) > dartlist

lint: $(DLINT) list

$(DLINT): list
	-flutter analyze --write=$(DLINT)
	sort $(DLINT)  -t : -n -k 1,2 -o $(DLINT)
	
tags: list $(DARTLIST)
	$(CTAGS) $(DARTLIST) | sort > tags
