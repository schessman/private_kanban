# private_kanban_app

Private Kanban Flutter application, no network traffic, snooping, ads, or other activity.

## Requirements

This app uses sqlite to persist tasks so there will be data stored on the phone.
The bloc and flutter\_bloc packages are being used to provide me with some experience in flutter/dart practices.

# dependencies
## BloC uses
- rxdart: ^0.18.1
- bloc: ^0.7.2
- flutter\_bloc: ^0.4.2
- sqflite: ^0.12.2+1

## credits
- https://pub.dartlang.org/packages/bloc for Felix Angelov's dart implementation of the BloC pattern.
- https://pub.dartlang.org/packages/flutter_bloc for Felix Angelov's flutter Widgets for the BloC pattern.
- https://grokonez.com/flutter/flutter-sqlite-example-listview-crud-operations-sqflite-plugin
for SQLite Flutter example



