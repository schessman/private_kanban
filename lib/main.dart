/// private_kanban: flutter kanban board with no network/cloud interface
import 'package:flutter/material.dart';
import 'package:bloc/bloc.dart';

// shared preferences
import 'package:shared_preferences/shared_preferences.dart';

// providers
import 'package:private_kanban/task/provider.dart';

// Import the app tasks
import 'package:private_kanban/page/app_page.dart';
import 'package:private_kanban/task/theme.dart';

const String appTitle = 'Private Kanban';

/// bloc transition printing for development
class SimpleBlocDelegate extends BlocDelegate {
  @override
  void onTransition(Transition<dynamic, dynamic> transition) {
    super.onTransition(transition);
    print(transition.toString());
  }

  @override
  void onError(Object error, StackTrace stacktrace) {
    super.onError(error, stacktrace);
    print(error);
  }
}

/// standard main() entry
/// async so shared preferences can load first
Future<void> main() async {
  // debugPrintRebuildDirtyWidgets = true;
  // prefs include current theme
  PrefsSingleton.prefs = await SharedPreferences.getInstance();
  // print bloc transitions
  BlocSupervisor().delegate = SimpleBlocDelegate();

  runApp(KanBanTheme());
}

/// KanBanTheme wraps app with ThemeProvider
class KanBanTheme extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ThemeProvider(
      child: KanBanApp(),
    );
  }
}

/// KanBanApp Application widget
/// Uses StreamBuilder to change themes
/// Shows AppPage which has Scaffold, AppBar, widgets
class KanBanApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final ThemeBloc themeBloc = ThemeProvider.of(context);
    // TODO(sam): this should use BlocBuilder(), https://gitlab.com/schessman/private_kanban/blob/master/lib/main.dart
    return StreamBuilder<ThemeData>(
      initialData: themeBloc.loadState(),
      stream: themeBloc.theme,
      builder: (BuildContext content, AsyncSnapshot<ThemeData> snapshot) {
        final ThemeData theme = snapshot.data;
        return MaterialApp(
          title: appTitle,
          theme: theme,
          home: AppPage(),
        );
      },
    );
  }
}
