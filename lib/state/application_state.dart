import 'package:meta/meta.dart';

enum AppStates {
  isInitializing,
  isLoading,
  isRunning,
  isProjects,
  isLabels,
  isTasks,
}

class ApplicationState {
  const ApplicationState({
    @required this.appState,
    @required this.title,
  });

  factory ApplicationState.initializing() {
    return const ApplicationState(
        appState: AppStates.isInitializing, title: 'Initializing');
  }

  factory ApplicationState.loading() {
    return const ApplicationState(
        appState: AppStates.isLoading, title: 'Loading');
  }

  factory ApplicationState.running() {
    return const ApplicationState(
        appState: AppStates.isRunning, title: 'Private Kanban');
  }

  factory ApplicationState.projects() {
    return const ApplicationState(
        appState: AppStates.isProjects, title: 'Projects');
  }

  factory ApplicationState.labels() {
    return const ApplicationState(
        appState: AppStates.isLabels, title: 'Labels');
  }

  factory ApplicationState.tasks() {
    return const ApplicationState(appState: AppStates.isTasks, title: 'Tasks');
  }

  ApplicationState copyWith({
    AppStates appState,
    String title,
  }) {
    return ApplicationState(appState: this.appState, title: this.title);
  }

  final AppStates appState;
  final String title;

  @override
  String toString() =>
      'ApplicationState { appState: $appState , title: $title, }';

  @override
  bool operator ==(Object other) =>
      other is ApplicationState && other.appState == appState;
  // Always override hashcode
  @override
  int get hashCode => appState.hashCode;
}
