import 'package:meta/meta.dart';

enum LabelStates {
  initial,
  addSuccess,
  addFail,
  delSuccess,
  delFail,
  renSuccess,
  renFail,
  failure,
}

class LabelState {
  const LabelState({
    @required this.labelSt,
  });

  factory LabelState.initial() {
    return const LabelState(labelSt: LabelStates.initial);
  }

  factory LabelState.addSuccess() {
    return const LabelState(labelSt: LabelStates.addSuccess);
  }

  factory LabelState.addFail() {
    return const LabelState(labelSt: LabelStates.addFail);
  }

  factory LabelState.delSuccess() {
    return const LabelState(labelSt: LabelStates.delSuccess);
  }

  factory LabelState.delFail() {
    return const LabelState(labelSt: LabelStates.delFail);
  }

  factory LabelState.renSuccess() {
    return const LabelState(labelSt: LabelStates.renSuccess);
  }

  factory LabelState.renFail() {
    return const LabelState(labelSt: LabelStates.renFail);
  }

  factory LabelState.failure(String error) {
    print('Label state failure: $error');
    return const LabelState(labelSt: LabelStates.failure);
  }

  final LabelStates labelSt;

  LabelState copyWith({LabelStates labelSt}) {
    return LabelState(labelSt: this.labelSt);
  }

  @override
  String toString() => 'LabelState { labelSt: $labelSt }';
}
