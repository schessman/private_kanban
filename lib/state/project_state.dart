import 'package:meta/meta.dart';
import 'package:flutter/material.dart';

import 'package:private_kanban/task/project.dart';

enum ProjectStates {
  initial,
  addSuccess,
  addFail,
  delSuccess,
  delFail,
  renSuccess,
  renFail,
  failure,
}

final ColorPalette initialColor = ColorPalette('Blue', Colors.blue.value);

class ProjectState {
  ProjectState({
    @required this.projectSt,
    @required this.colorPalette,
  });

  factory ProjectState.initial() {
    // set colorPalette?
    return ProjectState(
        projectSt: ProjectStates.initial, colorPalette: initialColor);
  }

  factory ProjectState.addSuccess() {
    return ProjectState(
        projectSt: ProjectStates.addSuccess, colorPalette: initialColor);
  }

  factory ProjectState.addFail() {
    return ProjectState(
        projectSt: ProjectStates.addFail, colorPalette: initialColor);
  }

  factory ProjectState.delSuccess() {
    return ProjectState(
        projectSt: ProjectStates.delSuccess, colorPalette: initialColor);
  }

  factory ProjectState.delFail() {
    return ProjectState(
        projectSt: ProjectStates.delFail, colorPalette: initialColor);
  }

  factory ProjectState.renSuccess() {
    return ProjectState(
        projectSt: ProjectStates.renSuccess, colorPalette: initialColor);
  }

  factory ProjectState.renFail() {
    return ProjectState(
        projectSt: ProjectStates.renFail, colorPalette: initialColor);
  }

  factory ProjectState.failure(String error) {
    print('Project state failure: $error');
    return ProjectState(
        projectSt: ProjectStates.failure, colorPalette: initialColor);
  }

  final ProjectStates projectSt;
  final ColorPalette colorPalette;
  bool hasData = false;
  List<Project> data = <Project>[];

  ProjectState copyWith({ProjectStates projectSt, ColorPalette colorp}) {
    return ProjectState(projectSt: this.projectSt, colorPalette: colorp);
  }

  @override
  String toString() =>
      'ProjectState { projectSt: $projectSt, colorPalette: $colorPalette }';
}
