import 'package:meta/meta.dart';

enum ThemeStates { initial, dark, light }

class ThemeState {
  const ThemeState({
    @required this.themeSt,
  });

  factory ThemeState.initial() {
    return const ThemeState(themeSt: ThemeStates.initial);
  }

  factory ThemeState.dark() {
    return const ThemeState(themeSt: ThemeStates.dark);
  }

  factory ThemeState.light() {
    return const ThemeState(themeSt: ThemeStates.light);
  }

  final ThemeStates themeSt;

  ThemeState copyWith({ThemeStates themeSt}) {
    return ThemeState(themeSt: this.themeSt);
  }

  @override
  String toString() => 'ThemeState { themeSt: $themeSt }';
}
