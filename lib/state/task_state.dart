import 'package:meta/meta.dart';

enum TaskStates {
  initial,
  addSuccess,
  addFail,
  delSuccess,
  delFail,
  renSuccess,
  renFail,
  failure,
}

class TaskState {
  const TaskState({
    @required this.taskSt,
  });

  factory TaskState.initial() {
    return const TaskState(taskSt: TaskStates.initial);
  }

  factory TaskState.addSuccess() {
    return const TaskState(taskSt: TaskStates.addSuccess);
  }

  factory TaskState.addFail() {
    return const TaskState(taskSt: TaskStates.addFail);
  }

  factory TaskState.delSuccess() {
    return const TaskState(taskSt: TaskStates.delSuccess);
  }

  factory TaskState.delFail() {
    return const TaskState(taskSt: TaskStates.delFail);
  }

  factory TaskState.renSuccess() {
    return const TaskState(taskSt: TaskStates.renSuccess);
  }

  factory TaskState.renFail() {
    return const TaskState(taskSt: TaskStates.renFail);
  }

  factory TaskState.failure(String error) {
    print('Task state failure: $error');
    return const TaskState(taskSt: TaskStates.failure);
  }

  final TaskStates taskSt;

  TaskState copyWith({TaskStates taskSt}) {
    return TaskState(taskSt: this.taskSt);
  }

  @override
  String toString() => 'TaskState { taskSt: $taskSt }';
}
