import 'package:flutter/material.dart';
import 'package:private_kanban/bloc/theme_bloc.dart';

class ThemeProvider extends InheritedWidget {
  ThemeProvider({Key key, Widget child}) : super(key: key, child: child);

  final ThemeBloc themeBloc = ThemeBloc();

  @override

  /// updateShouldNotify returns true if new theme
  bool updateShouldNotify(ThemeProvider oldWidget) =>
      oldWidget.themeBloc != themeBloc;

  /// ThemeBloc.of uses Inherited Widget to provide theme bloc
  static ThemeBloc of(BuildContext context) {
    final ThemeProvider _themeProvider =
        context.inheritFromWidgetOfExactType(ThemeProvider);
    return _themeProvider.themeBloc;
    // return (context.inheritFromWidgetOfExactType(ThemeProvider)
    //         as ThemeProvider)
    //     .themeBloc;
  }
}
