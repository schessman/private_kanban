/// private_kanban: flutter kanban board with no network/cloud interface
/// AppPage widget is the main App page
/// shows widgets based on ApplicationBloc states: initializing, loading, running
// show splash page while DB init
// todo: Use SliverAppBar for LanePage when AppStates.isRunning:
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:private_kanban/db/app_db.dart';
import 'package:private_kanban/task/application.dart';
import 'package:private_kanban/task/lane.dart';
import 'package:private_kanban/task/project.dart';
import 'package:private_kanban/task/splash.dart';
import 'package:private_kanban/task/theme.dart';
import 'package:private_kanban/widget/loading_indicator.dart';

/// AppPage()
/// handle application state
/// initialize DB singleton
class AppPage extends StatefulWidget {
  @override
  AppPageState createState() => AppPageState();
}

class AppPageState extends State<AppPage> {
  final ApplicationBloc _applicationBloc = ApplicationBloc(AppDatabase.get());

  // build handles the various states and their pages
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ApplicationEvent, ApplicationState>(
      bloc: _applicationBloc,
      builder: (BuildContext context, ApplicationState state) {
        final List<Widget> widgets = <Widget>[];

        // build a list of widgets to display based on appState
        switch (state.appState) {
          case AppStates.isInitializing:
            widgets.add(SplashPage());
            break;
          case AppStates.isLoading:
            widgets.add(loadingIndicator());
            break;
          case AppStates.isRunning:
            widgets.add(LanePage()); // this is the three column page
            break;
          case AppStates.isProjects:
            Navigator.push(
              context,
              MaterialPageRoute<bool>(
                  builder: (BuildContext context) => AddProjectPage()),
            );
            break;
          case AppStates.isLabels:
            break;
          case AppStates.isTasks:
            break;
          default:
            print(state.appState);
        }

        // return the list wrapped in a Scaffold(AppBar())
        // todo: use sliverappbar for animation
        return Scaffold(
          appBar: AppBar(
            title: Text(state.title),
            actions: <Widget>[
              ThemeIconButton(),
            ],
          ),
          body: Stack(
            children: widgets,
          ),
          bottomNavigationBar: BottomNavigationBar(
            onTap: _applicationBloc.onBBarTap,
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: Icon(Icons.playlist_add),
                title: Text('Project'),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.add_comment),
                title: Text('Label'),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.library_add),
                title: Text('Task'),
              )
            ],
          ),
          floatingActionButton: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 5.0),
                child: FloatingActionButton(
                  heroTag: 'add',
                  child: const Icon(Icons.add),
                  onPressed: _applicationBloc.onAppFAB,
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
