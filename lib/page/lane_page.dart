import 'package:flutter/material.dart';
import 'package:private_kanban/model/task_model.dart';
import 'package:private_kanban/widget/lane_widget.dart';

/// laneSortButton Widget to allow selection of sort order
Widget laneSortButton(String label) {
  String dropdownValue;
  return DropdownButton<String>(
    value: dropdownValue,
    hint: Text(label),
    onChanged: (String newValue) {},
    items: <String>[
      'Name',
      'DateDue',
      'DateOpened',
      'Oldest',
      'Project',
      'Label',
    ].map<DropdownMenuItem<String>>((String value) {
      return DropdownMenuItem<String>(
        value: value,
        child: Text(value),
      );
    }).toList(),
  );
}

/// LanePage returns Widget to display three KanBan Columns ToDo, Doing, Done
class LanePage extends StatefulWidget {
  @override
  LanePageState createState() => LanePageState();
}

class LanePageState extends State<LanePage> {
  // These three need to be dynamic based on sqlite queries
  final Widget todoSection = Expanded(
    child: Column(
      children: <Widget>[
        laneSortButton('Todo'),
        laneWidget(TaskStatus.TODO),
      ],
    ),
  );

  final Widget doingSection = Expanded(
    child: Column(
      children: <Widget>[
        laneSortButton('Doing'),
        laneWidget(TaskStatus.DOING),
      ],
    ),
  );
  final Widget doneSection = Expanded(
    child: Column(
      children: <Widget>[
        laneSortButton('Done'),
        laneWidget(TaskStatus.DONE),
      ],
    ),
  );

  @override
  Widget build(BuildContext context) {
    // final ApplicationBloc applicationBloc = BlocProvider.of<ApplicationBloc>(context);

    return /* Container( 
      child: */
        Row(
      children: <Widget>[
        todoSection,
        doingSection,
        doneSection,
      ],
      /* ), */
    );
  }
}
