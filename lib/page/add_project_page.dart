import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:private_kanban/task/project.dart';

class AddProjectPage extends StatelessWidget {
  final GlobalKey<CollapsibleExpansionTileState> expansionTile =
      GlobalKey<CollapsibleExpansionTileState>();
  final GlobalKey<FormState> _formState = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final ProjectBloc _projectBloc = BlocProvider.of(context);
    ColorPalette currentSelectedPalette;
    String projectName = '';
    return Scaffold(
      appBar: AppBar(
        title: const Text('Add Project'),
      ),
      floatingActionButton: FloatingActionButton(
          child: const Icon(
            Icons.send,
            color: Colors.white,
          ),
          onPressed: () {
            if (_formState.currentState.validate()) {
              _formState.currentState.save();
              final Project project = Project.create(
                  projectName,
                  currentSelectedPalette.colorValue,
                  currentSelectedPalette.colorName);
              _projectBloc.createProject(project);
              Navigator.pop(context, true);
            }
          }),
      body: ListView(
        children: <Widget>[
          Form(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                decoration: const InputDecoration(hintText: 'Project Name'),
                maxLength: 20,
                validator: (String value) {
                  return value.isEmpty ? 'Project name cannot be empty' : null;
                },
                onSaved: (String value) {
                  projectName = value;
                },
              ),
            ),
            key: _formState,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 4.0),
            child: BlocBuilder<ProjectEvent, ProjectState>(
              bloc: _projectBloc,
              builder: (
                BuildContext context,
                ProjectState state,
              ) {
                currentSelectedPalette = state.colorPalette;
                return CollapsibleExpansionTile(
                  key: expansionTile,
                  leading: Container(
                    width: 12.0,
                    height: 12.0,
                    child: CircleAvatar(
                      backgroundColor: Color(currentSelectedPalette.colorValue),
                    ),
                  ),
                  title: Text(currentSelectedPalette.colorName),
                  children: buildMaterialColors(_projectBloc),
                );
              },
            ),
          )
        ],
      ),
    );
  }

  List<Widget> buildMaterialColors(ProjectBloc projectBloc) {
    final List<Widget> projectWidgetList = <Widget>[];
    for (ColorPalette colors in colorsPalettes) {
      projectWidgetList.add(ListTile(
        leading: Container(
          width: 12.0,
          height: 12.0,
          child: CircleAvatar(
            backgroundColor: Color(colors.colorValue),
          ),
        ),
        title: Text(colors.colorName),
        onTap: () {
          expansionTile.currentState.collapse();
          projectBloc.updateColorSelection(
            ColorPalette(colors.colorName, colors.colorValue),
          );
        },
      ));
    }
    return projectWidgetList;
  }
}
