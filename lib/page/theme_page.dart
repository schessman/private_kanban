import 'package:flutter/material.dart';

import 'package:private_kanban/task/theme.dart';

class ThemeSelectorPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Theme Selector',
        ),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              ThemeMenu(),
            ],
          ),
        ),
      ),
    );
  }
}
