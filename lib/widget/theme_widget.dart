import 'package:flutter/material.dart';
import 'package:private_kanban/bloc/theme_bloc.dart';
import 'package:private_kanban/provider/theme_provider.dart';

class ThemeIconButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final ThemeBloc themeBloc = ThemeProvider.of(context);

    return StreamBuilder<ThemeType>(
      stream: themeBloc.themeType,
      builder: (BuildContext content, AsyncSnapshot<ThemeType> snapshot) {
        final ThemeType type = snapshot.data;
        final IconData icon =
            (type == ThemeType.light) ? Icons.brightness_4 : Icons.brightness_7;
        return IconButton(
          icon: Icon(icon),
          onPressed: () => themeBloc.flipTheme(),
        );
      },
    );
  }
}

class ThemeMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final ThemeBloc themeBloc = ThemeProvider.of(context);

    return StreamBuilder<ThemeType>(
      stream: themeBloc.themeType,
      builder: (BuildContext content, AsyncSnapshot<ThemeType> snapshot) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            RaisedButton(
              child: const Text('Light theme'),
              onPressed: themeBloc.onLight,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: RaisedButton(
                child: const Text('Dark theme'),
                onPressed: themeBloc.onDark,
              ),
            ),
          ],
        );
      },
    );
  }
}
