import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:private_kanban/task/project.dart';

class ProjectPage extends StatelessWidget {
  const ProjectPage({this.projectBloc});

  final ProjectBloc projectBloc;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProjectEvent, ProjectState>(
      bloc: projectBloc,
      builder: (
        BuildContext context,
        ProjectState state,
      ) {
        if (state.hasData) {
          return ProjectExpansionTileWidget(state.data);
        } else {
          return const CircularProgressIndicator();
        }
      },
    );
  }
}

class ProjectExpansionTileWidget extends StatelessWidget {
  const ProjectExpansionTileWidget(this._projects);

  final List<Project> _projects;

  @override
  Widget build(BuildContext context) {
    return ExpansionTile(
      leading: const Icon(Icons.book),
      title: const Text('Projects',
          style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold)),
      children: buildProjects(context),
    );
  }

  List<Widget> buildProjects(BuildContext context) {
    final List<Widget> projectWidgetList = <Widget>[];
    for (Project project in _projects) {
      projectWidgetList.add(ProjectRow(project));
    }
    projectWidgetList.add(ListTile(
      leading: const Icon(Icons.add),
      title: const Text('Add Project'),
      onTap: () async {
        await Navigator.push(
            context,
            MaterialPageRoute<bool>(
              builder: (BuildContext context) =>
                  AddProjectPage().build(context),
            ));
      },
    ));
    return projectWidgetList;
  }
}

class ProjectRow extends StatelessWidget {
  const ProjectRow(this.project);

  final Project project;

  @override
  Widget build(BuildContext context) {
    return const CircularProgressIndicator();
  }
}
