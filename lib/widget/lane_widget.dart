import 'package:flutter/material.dart';

import 'package:private_kanban/model/task_model.dart';

/// laneWidget(lane): show list of tasks for either TO-DO, DOING or DONE status
Widget laneWidget(TaskStatus lane) {
  final ScrollController controller = ScrollController();

  // TODO(sam): count db entries,
  const num _itemCount = 200;
  const num _itemExtent = 100.0;
  Icon _icon;
  switch (lane) {
    case TaskStatus.TODO:
      _icon = const Icon(Icons.inbox);
      break;
    case TaskStatus.DOING:
      _icon = const Icon(Icons.work);
      break;
    case TaskStatus.DONE:
      _icon = const Icon(Icons.done_outline);
      break;
    default:
      _icon = const Icon(Icons.device_unknown);
  }

  return Expanded(
    child: ListView.builder(
      shrinkWrap: true,
      controller: controller,
      itemCount: _itemCount,
      itemExtent: _itemExtent,
      itemBuilder: (BuildContext context, int index) {
        return Container(
          width: 20,
          height: 20,
          padding: const EdgeInsets.all(8.0),
          child: Material(
            elevation: 4.0,
            borderRadius: BorderRadius.circular(4.0),
            color: Colors.purple[index % 9 * 100],
            child: ListTile(
              leading: _icon,
              title: Text(index.toString()),
              // TODO(sam): route to appropriate form on tap,
              onTap: () {/* route here */},
            ),
          ),
        );
      },
    ),
  );
}
