/// loading indicator widget
import 'package:flutter/material.dart';

Widget loadingIndicator() {
  return Stack(
    children: const <Widget>[
      Opacity(
        opacity: 0.3,
        child: ModalBarrier(dismissible: false, color: Colors.grey),
      ),
      Center(
        child: CircularProgressIndicator(),
      ),
    ],
  );
}
