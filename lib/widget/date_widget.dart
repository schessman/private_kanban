import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

/// Date widgets
/// Future<String> chooseDate(context,initialDateString) async
/// onPressed:
Future<String> chooseDate(
    BuildContext context, String initialDateString) async {
  final DateTime now = DateTime.now();
  DateTime initialDate = convertToDate(initialDateString) ?? now;
  initialDate =
      initialDate.year >= 1900 && initialDate.isBefore(now) ? initialDate : now;

  final DateTime result = await showDatePicker(
      context: context,
      initialDate: initialDate,
      firstDate: DateTime(1900),
      lastDate: DateTime.now());

  if (result == null) {
    return initialDateString;
  }

  return DateFormat.yMd().format(result);
}

DateTime convertToDate(String input) {
  try {
    final DateTime d = DateFormat.yMd().parseStrict(input);
    return d;
  } catch (e) {
    return null;
  }
}
