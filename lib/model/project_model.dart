import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

class Project {
  Project.create(this.name, this.colorValue, this.colorName);

  Project.update(
      {@required this.id,
      String name,
      this.colorValue,
      String colorName = ''}) {
    if (name != '') {
      this.name = name;
    }
    if (colorName != '') {
      this.colorName = colorName;
    }
  }

  Project.fromMap(Map<String, dynamic> map)
      : this.update(
            id: map[dbId],
            name: map[dbName],
            colorValue: map[dbColorValue],
            colorName: map[dbColorName]);

  static const String tblProject = 'projects';
  static const String dbId = 'id';
  static const String dbName = 'name';
  static const String dbColorValue = 'colorValue';
  static const String dbColorName = 'colorName';

  int id, colorValue;
  String name, colorName;

  @override
  bool operator ==(Object other) => other is Project && other.id == id;
  // Always override hashcode
  @override
  int get hashCode => id.hashCode;
}
