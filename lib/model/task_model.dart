import 'package:meta/meta.dart';
import 'package:private_kanban/model/priority_model.dart';

/// TaskStatus - enum with state of task
enum TaskStatus {
  TODO,
  DOING,
  DONE,
}

class Tasks {
  Tasks.create(
      {@required this.title,
      @required this.projectId,
      this.comment = '',
      this.dueDate = -1,
      this.priority = Priority_level.PRIORITY_4}) {
    if (dueDate == -1) {
      dueDate = DateTime.now().millisecondsSinceEpoch;
    }
    tasksStatus = TaskStatus.TODO;
  }

  Tasks.update(
      {@required this.id,
      @required this.title,
      @required this.projectId,
      this.comment = '',
      this.dueDate = -1,
      this.priority = Priority_level.PRIORITY_4,
      this.tasksStatus = TaskStatus.DOING}) {
    if (dueDate == -1) {
      dueDate = DateTime.now().millisecondsSinceEpoch;
    }
  }

  Tasks.fromMap(Map<String, dynamic> map)
      : this.update(
          id: map[dbId],
          title: map[dbTitle],
          projectId: map[dbProjectID],
          comment: map[dbComment],
          dueDate: map[dbDueDate],
          priority: Priority_level.values[map[dbPriority]],
          tasksStatus: TaskStatus.values[map[dbStatus]],
        );

  static const String tblTask = 'tasks';
  static const String dbId = 'id';
  static const String dbTitle = 'title';
  static const String dbComment = 'comment';
  static const String dbDueDate = 'dueDate';
  static const String dbPriority = 'priority';
  static const String dbStatus = 'status';
  static const String dbProjectID = 'projectId';

  String title, comment, projectName;
  int id, dueDate, projectId, projectColor;
  Priority_level priority;
  TaskStatus tasksStatus;
  List<String> labelList = <String>[];

  /// == : compare Tasks by id
  @override
  bool operator ==(Object other) => other is Tasks && other.id == id;
  // Always override hashcode
  @override
  int get hashCode => id.hashCode;
}
