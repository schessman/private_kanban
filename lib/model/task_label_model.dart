class TaskLabels {
  TaskLabels.create(this.taskId, this.labelId);

  TaskLabels.update({this.id, this.taskId, this.labelId});

  TaskLabels.fromMap(Map<String, dynamic> map)
      : this.update(
            id: map[dbId], taskId: map[dbTaskId], labelId: map[dbLabelId]);

  static const String tblTaskLabel = 'taskLabel';
  static const String dbId = 'id';
  static const String dbTaskId = 'taskId';
  static const String dbLabelId = 'labelId';

  int id, taskId, labelId;
}
