import 'package:meta/meta.dart';

class Label {
  Label.create(this.name, this.colorValue, this.colorName);

  Label.update(
      {@required this.id,
      String name = '',
      this.colorValue,
      String colorName = ''}) {
    if (name != '') {
      this.name = name;
    }
    if (colorName != '') {
      this.colorName = colorName;
    }
  }

  Label.fromMap(Map<String, dynamic> map)
      : this.update(
            id: map[dbId],
            name: map[dbName],
            colorValue: map[dbColorValue],
            colorName: map[dbColorName]);

  static const String tblLabel = 'labels';
  static const String dbId = 'id';
  static const String dbName = 'name';
  static const String dbColorValue = 'colorValue';
  static const String dbColorName = 'colorName';

  int id, colorValue;
  String name, colorName;

  @override
  bool operator ==(Object other) => other is Label && other.id == id;
  // Always override hashcode
  @override
  int get hashCode => id.hashCode;
}
