import 'dart:async';

// import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';

import 'package:private_kanban/task/application.dart';
import 'package:private_kanban/db/app_db.dart';

class ApplicationBloc extends Bloc<ApplicationEvent, ApplicationState> {
  ApplicationBloc(AppDatabase appDatabase) {
    _appDatabase = appDatabase;
    print('ApplicationBloc appDatabase');
    loadState();
    dispatch(AppInitializing());
  }

  AppDatabase _appDatabase;

  void onAppStart() {
    dispatch(AppInitializing());
  }

  void onAppLoading() {
    dispatch(AppLoading());
  }

  void onAppRunning() {
    dispatch(AppRunning());
  }

  void onAppFAB() {
    dispatch(AppFAB());
  }

  void onBBarTap(int index) {
    dispatch(AppMenu(index: index));
  }

  @override
  ApplicationState get initialState => ApplicationState.initializing();

  @override
  Stream<ApplicationState> mapEventToState(ApplicationEvent event) async* {
    // print(state.appState); // debug
    // print(event); // debug
    if (event is AppMenu) {
      switch (event.index) {
        case 0:
          print('Projects');
          yield ApplicationState.projects();
          break;
        case 1:
          print('Labels');
          yield ApplicationState.labels();
          break;
        case 2:
          print('Tasks');
          yield ApplicationState.tasks();
          break;
        default:
          print('AppMenu');
          print(event.index);
          break;
      }
    } else if (event is AppInitializing) {
      // yield ApplicationState.initializing();
      await _appDatabase.getDb();
      yield ApplicationState.loading();
      await _delay(5);
      yield ApplicationState.running();
    } else if (event is AppFAB) {
      switch (currentState.appState) {
        case AppStates.isInitializing:
          yield ApplicationState.loading();
          await _appDatabase.getDb();
          yield ApplicationState.running();
          break;
        case AppStates.isLoading:
          yield ApplicationState.running();
          break;
        case AppStates.isRunning:
          yield ApplicationState.initializing();
          await _appDatabase.getDb();
          yield ApplicationState.loading();
          await _delay(5);
          yield ApplicationState.running();
          break;
        default:
          print(currentState.appState);
      }
    } else {
      print(event);
    }
  }

  Future<bool> _delay(int sec) async {
    /// open db
    await Future<Timer>.delayed(Duration(seconds: sec));
    return true;
  }

  void loadState() {}
  // Future saveState() async {}
  // @override
  // Future dispose() async {}
}
