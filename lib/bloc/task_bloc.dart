import 'dart:async';

// import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:private_kanban/task/task.dart';

class TaskBloc extends Bloc<TaskEvent, TaskState> {
  @override
  TaskState get initialState => TaskState.initial();

  // could use logic to see if name already exists
  // three actions: add, del, rename
  void onAddButtonPressed({String taskname}) {
    dispatch(
      AddButtonPressed(
        taskname: taskname,
      ),
    );
  }

  void onDelButtonPressed({String taskname}) {
    dispatch(
      DelButtonPressed(
        taskname: taskname,
      ),
    );
  }

  void onRenButtonPressed({String taskname}) {
    dispatch(
      RenButtonPressed(
        taskname: taskname,
      ),
    );
  }

  @override
  Stream<TaskState> mapEventToState(TaskEvent event) async* {
    if (event is AddButtonPressed) {
      try {
        // getDb, task.add name
        final bool txnSucceeded = await _addTask(name: event.taskname);
        if (txnSucceeded) {
          yield TaskState.addSuccess();
        } else {
          yield TaskState.addFail();
        }
      } catch (error) {
        yield TaskState.failure(error.toString());
      }
    }
    if (event is DelButtonPressed) {
      try {
        // getDb, task.add name
        final bool txnSucceeded = await _delTask(name: event.taskname);
        if (txnSucceeded) {
          yield TaskState.delSuccess();
        } else {
          yield TaskState.delFail();
        }
      } catch (error) {
        yield TaskState.failure(error.toString());
      }
    }
    if (event is RenButtonPressed) {
      try {
        // getDb, task.add name
        final bool txnSucceeded = await _renTask(name: event.taskname);
        if (txnSucceeded) {
          yield TaskState.renSuccess();
        } else {
          yield TaskState.renFail();
        }
      } catch (error) {
        yield TaskState.failure(error.toString());
      }
    }
  }

  Future<bool> _addTask({
    String name,
  }) async {
    await Future<Timer>.delayed(Duration(seconds: 1));
    return true;
  }

  Future<bool> _delTask({
    String name,
  }) async {
    await Future<Timer>.delayed(Duration(seconds: 1));
    return true;
  }

  Future<bool> _renTask({
    String name,
  }) async {
    await Future<Timer>.delayed(Duration(seconds: 1));
    return true;
  }
}
