import 'dart:async';

// import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:private_kanban/task/project.dart';

class ProjectBloc extends Bloc<ProjectEvent, ProjectState> {
  ProjectBloc({this.colorSelection});
  @override
  ProjectState get initialState => ProjectState.initial();
  final ColorPalette colorSelection;

  void createProject(Project project) {}

  // could use logic to see if name already exists
  // three actions: add, del, rename
  void onAddButtonPressed({String projectname}) {
    dispatch(
      AddButtonPressed(
        projectname: projectname,
      ),
    );
  }

  void onDelButtonPressed({String projectname}) {
    dispatch(
      DelButtonPressed(
        projectname: projectname,
      ),
    );
  }

  void onRenButtonPressed({String projectname}) {
    dispatch(
      RenButtonPressed(
        projectname: projectname,
      ),
    );
  }

  void updateColorSelection(ColorPalette colorPalette) {
    dispatch(AddColor(colorPalette: colorPalette));
  }

  void addProject() {
    dispatch(AddProject());
  }

  @override
  Stream<ProjectState> mapEventToState(ProjectEvent event) async* {
    if (event is AddButtonPressed) {
      try {
        // getDb, project.add name
        final bool txnSucceeded = await _addProject(name: event.projectname);
        if (txnSucceeded) {
          yield ProjectState.addSuccess();
        } else {
          yield ProjectState.addFail();
        }
      } catch (error) {
        yield ProjectState.failure(error.toString());
      }
    }
    if (event is DelButtonPressed) {
      try {
        // getDb, project.add name
        final bool txnSucceeded = await _delProject(name: event.projectname);
        if (txnSucceeded) {
          yield ProjectState.delSuccess();
        } else {
          yield ProjectState.delFail();
        }
      } catch (error) {
        yield ProjectState.failure(error.toString());
      }
    }
    if (event is RenButtonPressed) {
      try {
        // getDb, project.add name
        final bool txnSucceeded = await _renProject(name: event.projectname);
        if (txnSucceeded) {
          yield ProjectState.renSuccess();
        } else {
          yield ProjectState.renFail();
        }
      } catch (error) {
        yield ProjectState.failure(error.toString());
      }
    }
  }

  Future<bool> _addProject({
    String name,
  }) async {
    await Future<Timer>.delayed(Duration(seconds: 1));
    return true;
  }

  Future<bool> _delProject({
    String name,
  }) async {
    await Future<Timer>.delayed(Duration(seconds: 1));
    return true;
  }

  Future<bool> _renProject({
    String name,
  }) async {
    await Future<Timer>.delayed(Duration(seconds: 1));
    return true;
  }
}
