import 'dart:async';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:private_kanban/provider/prefs_singleton.dart';

// import 'package:private_kanban/task/theme.dart';

enum ThemeType { light, dark }

class ThemeBloc {
  ThemeBloc() {
    loadState();
  }

  // persistence
  final SharedPreferences prefs = PrefsSingleton.prefs;
  static const String _prefThemeTypeName = 'theme_type';

  // rxdart variables
  final PublishSubject<ThemeData> _theme = PublishSubject<ThemeData>();
  final BehaviorSubject<ThemeType> _themeType = BehaviorSubject<ThemeType>();

  // Streams
  Observable<ThemeData> get theme => _theme.stream;
  Observable<ThemeType> get themeType => _themeType.stream;

  // Sinks
  Function(ThemeData) get _changeTheme => _theme.sink.add;
  Function(ThemeType) get _changeThemeType => _themeType.sink.add;

  // Page Functions
  void onLight() {
    changeTheme(ThemeType.light);
  }

  void onDark() {
    changeTheme(ThemeType.dark);
  }

  // Logic Functions
  Future<void> changeTheme(ThemeType type) async {
    final ThemeData theme = themeTypeToData(type);
    _changeTheme(theme);
    _changeThemeType(type);
    await saveState(type);
  }

  void flipTheme() {
    final ThemeType newTheme =
        (_themeType.value == ThemeType.dark) ? ThemeType.light : ThemeType.dark;
    changeTheme(newTheme);
  }

  /// themeTypeToData (ThemeType) returns associated ThemeData
  ThemeData themeTypeToData(ThemeType type) {
    return (type == ThemeType.light) ? ThemeData.light() : ThemeData.dark();
  }

  /// themeDataToType (ThemeData) returns associated ThemeType
  ThemeType themeDataToType(ThemeData theme) {
    return (theme == ThemeData.light()) ? ThemeType.light : ThemeType.dark;
  }

  // Persistence Functions
  Future<void> saveState(ThemeType themeType) async {
    await prefs.setString(_prefThemeTypeName, _themeType.value.toString());
  }

  ThemeData loadState() {
    final String themePref =
        prefs.getString(_prefThemeTypeName) ?? ThemeType.light.toString();
    final ThemeType type = (themePref == ThemeType.light.toString())
        ? ThemeType.light
        : ThemeType.dark;
    _changeThemeType(type);
    final ThemeData theme = themeTypeToData(type);
    _changeTheme(theme);
    return theme;
  }

  Future<void> dispose() async {
    // cleanup
    _theme.close();
    _themeType.close();
  }
}
