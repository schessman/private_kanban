import 'dart:async';

// import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:private_kanban/task/label.dart';

class LabelBloc extends Bloc<LabelEvent, LabelState> {
  @override
  LabelState get initialState => LabelState.initial();

  // could use logic to see if name already exists
  // three actions: add, del, rename
  void onAddButtonPressed({String labelname}) {
    dispatch(
      AddButtonPressed(
        labelname: labelname,
      ),
    );
  }

  void onDelButtonPressed({String labelname}) {
    dispatch(
      DelButtonPressed(
        labelname: labelname,
      ),
    );
  }

  void onRenButtonPressed({String labelname}) {
    dispatch(
      RenButtonPressed(
        labelname: labelname,
      ),
    );
  }

  @override
  Stream<LabelState> mapEventToState(LabelEvent event) async* {
    if (event is AddButtonPressed) {
      try {
        // getDb, label.add name
        final bool txnSucceeded = await _addLabel(name: event.labelname);
        if (txnSucceeded) {
          yield LabelState.addSuccess();
        } else {
          yield LabelState.addFail();
        }
      } catch (error) {
        yield LabelState.failure(error.toString());
      }
    }
    if (event is DelButtonPressed) {
      try {
        // getDb, label.add name
        final bool txnSucceeded = await _delLabel(name: event.labelname);
        if (txnSucceeded) {
          yield LabelState.delSuccess();
        } else {
          yield LabelState.delFail();
        }
      } catch (error) {
        yield LabelState.failure(error.toString());
      }
    }
    if (event is RenButtonPressed) {
      try {
        // getDb, label.add name
        final bool txnSucceeded = await _renLabel(name: event.labelname);
        if (txnSucceeded) {
          yield LabelState.renSuccess();
        } else {
          yield LabelState.renFail();
        }
      } catch (error) {
        yield LabelState.failure(error.toString());
      }
    }
  }

  Future<bool> _addLabel({
    String name,
  }) async {
    await Future<Timer>.delayed(Duration(seconds: 1));
    return true;
  }

  Future<bool> _delLabel({
    String name,
  }) async {
    await Future<Timer>.delayed(Duration(seconds: 1));
    return true;
  }

  Future<bool> _renLabel({
    String name,
  }) async {
    await Future<Timer>.delayed(Duration(seconds: 1));
    return true;
  }
}
