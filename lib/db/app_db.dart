import 'dart:async';
import 'dart:io';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

import 'package:private_kanban/task/model.dart';

/// class AppDatabase() - singleton database class
/// getDb() returns singleton opened database "private_kanban.db", waits for _init().
/// onCreate creates four tables: "project", "task", "task_label", "label".
class AppDatabase {
  //private internal constructor to make it singleton
  AppDatabase._internal();

  static final AppDatabase _appDatabase = AppDatabase._internal();

  Database _database;

  static AppDatabase get() {
    return _appDatabase;
  }

  bool _didInit = false;

  /// Future <Database> getDb() async returns singleton opened/initialized DB.
  Future<Database> getDb() async {
    if (!_didInit) {
      await _init();
    }
    return _database;
  }

  /// Future <Database> close() async returns void
  Future<void> close() async {
    if (_didInit) {
      await _database.close();
      _database = null;
    }
    return;
  }

  Future<void> _init() async {
    // Get a location using path_provider
    try {
      final Directory documentsDirectory =
          await getApplicationDocumentsDirectory();
      final String path = join(documentsDirectory.path, 'private_kanban.db');
      _database = await openDatabase(path, version: 1,
          onCreate: (Database db, int version) async {
        // When creating the db, create the table
        await _createProjectTable(db);
        await _createTaskTable(db);
        await _createLabelTable(db);
      }, onUpgrade: (Database db, int oldVersion, int newVersion) async {
        // drop in dependent order
        await db.execute('DROP TABLE ${TaskLabels.tblTaskLabel}');
        await db.execute('DROP TABLE ${Label.tblLabel}');
        await db.execute('DROP TABLE ${Tasks.tblTask}');
        await db.execute('DROP TABLE ${Project.tblProject}');
        await _createProjectTable(db);
        await _createTaskTable(db);
        await _createLabelTable(db);
      });
      _didInit = true;
    } catch (error) {
      _didInit = false; // for unit testing
    }
  }

  Future<void> _createProjectTable(Database db) {
    return db.execute('CREATE TABLE ${Project.tblProject} ('
        '${Project.dbId} INTEGER PRIMARY KEY AUTOINCREMENT,'
        '${Project.dbName} TEXT,'
        '${Project.dbColorName} TEXT,'
        '${Project.dbColorValue} INTEGER);');
  }

  Future<void> _createTaskTable(Database db) {
    return db.execute('CREATE TABLE ${Tasks.tblTask} ('
        '${Tasks.dbId} INTEGER PRIMARY KEY AUTOINCREMENT,'
        '${Tasks.dbTitle} TEXT,'
        '${Tasks.dbComment} TEXT,'
        '${Tasks.dbDueDate} LONG,'
        '${Tasks.dbPriority} LONG,'
        '${Tasks.dbProjectID} LONG,'
        '${Tasks.dbStatus} LONG,'
        'FOREIGN KEY(${Tasks.dbProjectID}) REFERENCES ${Project.tblProject}(${Project.dbId}) ON DELETE CASCADE);');
  }

  Future<void> _createLabelTable(Database db) {
    return db.transaction((Transaction txn) async {
      txn.execute('CREATE TABLE ${Label.tblLabel} ('
          '${Label.dbId} INTEGER PRIMARY KEY AUTOINCREMENT,'
          '${Label.dbName} TEXT,'
          '${Label.dbColorName} TEXT,'
          '${Label.dbColorValue} INTEGER);');

      txn.execute('CREATE TABLE ${TaskLabels.tblTaskLabel} ('
          '${TaskLabels.dbId} INTEGER PRIMARY KEY AUTOINCREMENT,'
          '${TaskLabels.dbTaskId} INTEGER,'
          '${TaskLabels.dbLabelId} INTEGER,'
          'FOREIGN KEY(${TaskLabels.dbTaskId}) REFERENCES ${Tasks.tblTask}(${Tasks.dbId}) ON DELETE CASCADE,'
          'FOREIGN KEY(${TaskLabels.dbLabelId}) REFERENCES ${Label.tblLabel}(${Label.dbId}) ON DELETE CASCADE);');
    });
  }
}
