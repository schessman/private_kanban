// import 'package:meta/meta.dart';

abstract class ThemeEvent {}

class ThemeInitial extends ThemeEvent {}

class ThemeLight extends ThemeEvent {}

class ThemeDark extends ThemeEvent {}
