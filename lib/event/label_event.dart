// import 'package:meta/meta.dart';

abstract class LabelEvent {}

class AddButtonPressed extends LabelEvent {
  AddButtonPressed({this.labelname});

  final String labelname;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AddButtonPressed &&
          runtimeType == other.runtimeType &&
          labelname == other.labelname;

  @override
  String toString() => 'AddButtonPressed { labelname: $labelname }';

  @override
  int get hashCode => labelname.hashCode;
}

class DelButtonPressed extends LabelEvent {
  DelButtonPressed({this.labelname});

  final String labelname;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is DelButtonPressed &&
          runtimeType == other.runtimeType &&
          labelname == other.labelname;

  @override
  String toString() => 'DelButtonPressed { labelname: $labelname }';

  @override
  int get hashCode => labelname.hashCode;
}

class RenButtonPressed extends LabelEvent {
  RenButtonPressed({this.labelname});

  final String labelname;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is RenButtonPressed &&
          runtimeType == other.runtimeType &&
          labelname == other.labelname;

  @override
  String toString() => 'RenButtonPressed { labelname: $labelname }';

  @override
  int get hashCode => labelname.hashCode;
}
