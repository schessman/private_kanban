// import 'package:meta/meta.dart';

abstract class ApplicationEvent {}

class AppInitializing extends ApplicationEvent {}

class AppLoading extends ApplicationEvent {}

class AppRunning extends ApplicationEvent {}

class AppFAB extends ApplicationEvent {}

class AppMenu extends ApplicationEvent {
  AppMenu({this.index});
  final int index;
}
