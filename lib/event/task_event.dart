// import 'package:meta/meta.dart';

abstract class TaskEvent {}

class AddButtonPressed extends TaskEvent {
  AddButtonPressed({this.taskname});

  final String taskname;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AddButtonPressed &&
          runtimeType == other.runtimeType &&
          taskname == other.taskname;

  @override
  String toString() => 'AddButtonPressed { taskname: $taskname }';

  @override
  int get hashCode => taskname.hashCode;
}

class DelButtonPressed extends TaskEvent {
  DelButtonPressed({this.taskname});

  final String taskname;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is DelButtonPressed &&
          runtimeType == other.runtimeType &&
          taskname == other.taskname;

  @override
  String toString() => 'DelButtonPressed { taskname: $taskname }';

  @override
  int get hashCode => taskname.hashCode;
}

class RenButtonPressed extends TaskEvent {
  RenButtonPressed({this.taskname});

  final String taskname;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is RenButtonPressed &&
          runtimeType == other.runtimeType &&
          taskname == other.taskname;

  @override
  String toString() => 'RenButtonPressed { taskname: $taskname }';

  @override
  int get hashCode => taskname.hashCode;
}
