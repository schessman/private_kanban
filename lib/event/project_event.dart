// import 'package:meta/meta.dart';
import 'package:private_kanban/task/project.dart';

abstract class ProjectEvent {}

class AddButtonPressed extends ProjectEvent {
  AddButtonPressed({this.projectname});

  final String projectname;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AddButtonPressed &&
          runtimeType == other.runtimeType &&
          projectname == other.projectname;

  @override
  String toString() => 'AddButtonPressed { projectname: $projectname }';

  @override
  int get hashCode => projectname.hashCode;
}

class AddProject extends ProjectEvent {}

class DelButtonPressed extends ProjectEvent {
  DelButtonPressed({this.projectname});

  final String projectname;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is DelButtonPressed &&
          runtimeType == other.runtimeType &&
          projectname == other.projectname;

  @override
  String toString() => 'DelButtonPressed { projectname: $projectname }';

  @override
  int get hashCode => projectname.hashCode;
}

class RenButtonPressed extends ProjectEvent {
  RenButtonPressed({this.projectname});

  final String projectname;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is RenButtonPressed &&
          runtimeType == other.runtimeType &&
          projectname == other.projectname;

  @override
  String toString() => 'RenButtonPressed { projectname: $projectname }';

  @override
  int get hashCode => projectname.hashCode;
}

class AddColor extends ProjectEvent {
  AddColor({this.colorPalette});

  final ColorPalette colorPalette;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AddColor &&
          runtimeType == other.runtimeType &&
          colorPalette == other.colorPalette;

  @override
  String toString() => 'AddColor { colorPalette: $colorPalette }';

  @override
  int get hashCode => colorPalette.hashCode;
}
